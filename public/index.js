let romanNames;

window.onload = async () => {
    romanNames = await fetchRomanNames();
    greekNames = await fetchGreekNames();
}

async function generateNames() {
    generateRomanName();
    generateGreekName();
};

async function generateRomanName() {
    const firstName = getRandomName(romanNames.firstNames);
    const familyName = getRandomName(romanNames.familyNames);
    const nickname = getRandomName(romanNames.nicknames);
    document.getElementById("generatedRomanName").innerHTML = concatNames(firstName, familyName, nickname);
}

async function generateGreekName() {
    const maleName = getRandomName(greekNames.male);
    const femaleName = getRandomName(greekNames.female);
    document.getElementById("generatedGreekMaleName").innerHTML = maleName;
    document.getElementById("generatedGreekFemaleName").innerHTML = femaleName;
};

async function fetchRomanNames() {
    return fetch("./data/romanNames.json")
        .then((response) => response.json())
        .then((romanNames) => romanNames);
}

async function fetchGreekNames() {
    return fetch("./data/greekNames.json")
    .then((response) => response.json())
    .then((greekNames) => greekNames);
}

function getRandomName(names) {
    return names[getRandomIndex(names.length)];
}

function concatNames(...names) {
    return names.reduce((accumulator, currentValue) => {
        return accumulator + " " + currentValue;
    })
}

function getRandomIndex(max) {
    return Math.floor(Math.random() * max);
}